<?php
namespace App\Controller;

use App\Controller\AppController;

class ArticlesController extends AppController
{
    // public $paginate = [
    //         'order' => [
    //             'Articles.id' => 'asc'
    //         ]
    //     ];
    public function initialize()
    {
        parent::initialize();

        $this->Auth->allow(['tags']);
        $this->loadComponent('Paginator');
        $this->loadComponent('Flash');
        
    }

    public function index()
    {
        $list_articles = $this->Articles->find();
        $articles = $this->Paginator->paginate($list_articles, ['limit'=> 3]);
        $this->set(compact('articles'));
    }

    public function view($slug)
    {
        $article = $this->Articles->findById($slug)->firstOrFail();
        $this->set(compact('article'));
    }

    public function add()
    {
        $article = $this->Articles->newEntity();    //trỏ tới bảng Articles -> hàm tạo 1 thực thể mới
        if ($this->request->is('post')) {   //kiểm tra xem có dữ liệu submit hay ko
            $article = $this->Articles->patchEntity($article, $this->request->getData());   //patchEntity lưu dữ liệu (vào bảng article, yêu cầu lấy dữ liệu)
            $article->user_id = $this->Auth->user('id');

            if ($this->Articles->save($article)) {  //nếu lưu dữ liệu thành công
                $this->Flash->success(__('Your article has been saved.'));  //hiện thông báo 
                return $this->redirect(['action' => 'index']);  //điều hương về index
            }
            $this->Flash->error(__('Unable to add your article.')); //thông báo lỗi
        }
        $this->set('article', $article);

        $categories = $this->Articles->Categories->find('treeList');
        $this->set(compact('categories'));
    }

    public function edit($slug)
	{
	    $article = $this->Articles->findById($slug)->contain('Tags')->firstOrFail();    //findById tìm theo Id->hiển thị kết quả duy nhất
	    if ($this->request->is(['post', 'put'])) {
	        $this->Articles->patchEntity($article, $this->request->getData(),
                ['accessibleFields' => ['user_id' => false]
        ]);
	        if ($this->Articles->save($article)) {
	            $this->Flash->success(__('Your article has been updated.'));
	            return $this->redirect(['action' => 'index']);
	        }
	        $this->Flash->error(__('Unable to update your article.'));
	    }

	    $this->set('article', $article);// set tên biến $article ở controller truyền sang view & tên biến là article
	}

    public function delete($slug) {
    	$this->request->allowMethod(['post','delete']); //hàm này chỉ chạy khi nhận đc request post hoặc delete

    	$article = $this->Articles->findById($slug)->firstOrFail();
    	if($this->Articles->delete($article)){
    		$this->Flash->success(__('The {0} article has been delete.',$article->title));
    		return $this->redirect(['action' => 'index']);
    	}

    }
    public function isAuthorized($user)
    {
        if ($this->request->getParam('action') === 'add') {
            return true;
        }

        if (in_array($this->request->getParam('action'), ['edit', 'delete'])) {
            $articleId = (int)$this->request->getParam('pass.0');
            if ($this->Articles->isOwnedBy($articleId, $user['id'])) {
                return true;
            }
        }
        return parent::isAuthorized($user);
    }
}
?>
