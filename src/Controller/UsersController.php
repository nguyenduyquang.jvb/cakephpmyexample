<?php
namespace App\Controller;
use App\Controller\AppController;

class UsersController extends AppController
{
	public function initialize()
    {
        parent::initialize();

        $this->Auth->allow(['tags']);
        $this->loadComponent('Paginator');
        $this->loadComponent('Flash');
        
    }
	public function login(){
		if($this->request->is('post')){
			$user = $this->Auth->identify();
			if($user){
				$this->Auth->setUser($user);
				return $this->redirect($this->Auth->redirectUrl());
			}
			$this->Flash->error(__('Invalid username or password, try again'));
		}
	}


	public function index(){
		$list_user = $this->Users->find();
        $users = $this->Paginator->paginate($list_user, ['limit'=> 3]);
        $this->set(compact('users'));
	}

	public function view($id){
		$users = $this->Users->get($id);
		$this->set(compact('users'));
	}

	public function add(){
		$user = $this->Users->newEntity();
		if ($this->request->is('post')){
			$user = $this->Users->patchEntity($user,$this->request->getData());
			if($this->Users->save($user)){
				$this->Flash->success(__('The user has been saved.'));
				return $this->redirect(['action' => 'add']);
			}
			$this->Flash->error(__('Unable to add the user.'));
		}
		$this->set('user', $user);
	}

	public function edit($slug){
	    $users = $this->Users->findById($slug)->contain('Tags')->firstOrFail();    //findById tìm theo Id->hiển thị kết quả duy nhất
	    if ($this->request->is(['post', 'put'])) {
	        $this->Users->patchEntity($users, $this->request->getData(),
                ['accessibleFields' => ['user_id' => false]
        ]);
	        if ($this->Users->save($user)) {
	            $this->Flash->success(__('Your user has been updated.'));
	            return $this->redirect(['action' => 'index']);
	        }
	        $this->Flash->error(__('Unable to update your user.'));
	    }

	    $this->set('users', $users);// set tên biến $article ở controller truyền sang view & tên biến là article
	}

 	public function delete($slug) {
    	$this->request->allowMethod(['post','delete']); //hàm này chỉ chạy khi nhận đc request post hoặc delete

    	$user = $this->Users->findById($slug)->firstOrFail();
    	if($this->Users->delete($user)){
    		$this->Flash->success(__('The {0} user has been delete.',$user->username));
    		return $this->redirect(['action' => 'index']);
    	}

    }

	public function logout()
	{
		$this->Flash->success('You are now logged out.');
		return $this->redirect($this->Auth->logout());
	}
}
?>