<div class="users form">
	<?php echo $this->Flash->render(); ?>
	<?php echo $this->Form->create(); ?>
		<fieldset>
			<legend><?php echo ('Please enter your name and password.'); ?> </legend>
			<?php echo $this->Form->control('username'); ?>
			<?php echo $this->Form->control('password'); ?>
		</fieldset>
		<?php echo $this->Form->button(__('Login')); ?>
		<?php echo $this->Form->end(); ?>
</div>