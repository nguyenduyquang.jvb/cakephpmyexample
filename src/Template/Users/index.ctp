<h1>Articles</h1>
<p><?= $this->Html->link("Add Article", ['action'=>'add']) ?></p>
<table>
	<tr>
		<th>Username</th>
		<th>Password</th>
		<th>Action</th>
	</tr>
	<?php foreach($users as $user): ?>
	<tr>
		<td>
			<?= $this->Html->link($user->username, ['action'=>'view' ,$user->id]) ?>
		</td>
		<td>
			<?= $this->Html->link($user->password, ['action'=>'view' ,$user->id]) ?>
		</td>
		<td>
			<?= $this->Html->link('Edit',['action'=>'edit', $user->id]) ?>
			<?= $this->Form->postLink(
				'Delete',
				['action' => 'delete', $user->id],
				['confirm','Are you sure?']) 
			?>
		</td>
	</tr>
	<?php endforeach; ?>

</table>
	<?php echo $this->Paginator->prev(' << ' . __('Previous')); ?>
	<?php echo $this->Paginator->numbers(); ?>
	<?php echo $this->Paginator->next(' >> ' . __('Next')); ?>
	<?= $this->Paginator->counter() ?>
