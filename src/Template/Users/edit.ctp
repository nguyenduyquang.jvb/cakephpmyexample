<h1>Edit User</h1>
<?php
	echo $this->Form->create($user);
	echo $this->Form->control(['type'=>'hidden']);
	echo $this->Form->control('username');
	echo $this->Form->control('password');
	echo $this->Form->submit(__('Save User'));
	echo $this->Form->end();
?>