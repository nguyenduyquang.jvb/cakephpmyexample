<h1>Articles</h1>
<p><?= $this->Html->link("Add Article", ['action'=>'add']) ?></p>
<table>
	<tr>
		<th>Title</th>
		<th>Body</th>
		<th>Action</th>
	</tr>
	<?php foreach($articles as $article): ?>
	<tr>
		<td>
			<?= $this->Html->link($article->title, ['action'=>'view' ,$article->id]) ?>
		</td>
		<td>
			<?= $this->Html->link($article->body, ['action'=>'view' ,$article->id]) ?>
		</td>
		<td>
			<?= $this->Html->link('Edit',['action'=>'edit', $article->id]) ?>
			<?= $this->Form->postLink(
				'Delete',
				['action' => 'delete', $article->id],
				['confirm','Are you sure?']) 
			?>
		</td>
	</tr>
	<?php endforeach; ?>

</table>
	<?php echo $this->Paginator->prev(' << ' . __('Previous')); ?>
	<?php echo $this->Paginator->numbers(); ?>
	<?php echo $this->Paginator->next(' >> ' . __('Next')); ?>
	<?= $this->Paginator->counter() ?>
