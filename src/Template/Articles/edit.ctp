<h1>Edit Article</h1>
<?php
	echo $this->Form->create($article);
	echo $this->Form->control(['type'=>'hidden']);
	echo $this->Form->control('title');
	echo $this->Form->control('body',['row' => '3']);
	echo $this->Form->submit(__('Save Article'));
	echo $this->Form->end();
?>